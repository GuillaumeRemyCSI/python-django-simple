#!/usr/bin/env bash


python manage.py migrate
echo "server running at port 8000"
echo "Author = $AUTHOR"
sed -i "s/^ALLOWED_HOSTS = \[.*/ALLOWED_HOSTS = ['$PLAYWD-8080.direct.labs.play-with-docker.com']/" config/settings.py
python manage.py runserver 0.0.0.0:8000

